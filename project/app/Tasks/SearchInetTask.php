<?php
declare(strict_types=1);

namespace App\Tasks;

use Phalcon\Cli\Task;
use App\Common\Components\SearchInet\SearchInet;

/** /@noinspection PhpUnused*/
class SearchInetTask extends Task
{
    /** @noinspection PhpUnused
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function updateAction()
    {
        (new SearchInet($this->getDI()))->update();
    }

    /** @noinspection PhpUnused */
    public function testAction()
    {
        echo 'this is the test' . PHP_EOL;
    }

    /** @noinspection PhpUnused */
    public function deleteKeyAction()
    {
        (new SearchInet($this->getDI()))->deleteRedisKey();
        echo "Redis key was deleted\n";
    }

    /** @noinspection PhpUnused */
    public function statusAction()
    {
        echo sprintf("Update is %s\n", (new SearchInet($this->getDI()))->isRunning() ? 'running' : 'stopped');
    }
}