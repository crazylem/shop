<?php
declare(strict_types=1);

namespace App;

use Phalcon\Cli\Console;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Mvc\{ Application, Micro };
use App\Common\Providers\{ConfigProvider, DbProvider, LoggerProvider};
use Dotenv\Dotenv;

abstract class BootstrapBase
{
    protected Micro | Console | Application $_app;

    protected const LOGGER_FILENAME = 'main';

    public function getApplication(): null | Micro | Console | Application
    {
        return $this->_app ?? null;
    }

    public function initialize(): void
    {
        Dotenv::createImmutable(DIR_BASE)->load();
        $this->_setupApplication();
        $this->_initializeProviders();
    }

    protected function _initializeProviders(): void
    {
        $container = $this->getApplication()->getDI();
        foreach (array_merge(static::_getBaseProviders(), static::_getAdditionalProviders()) as $providerClass) {
            if (is_array($providerClass)) {
                $className = $providerClass['className'];
                /** @var ServiceProviderInterface $provider */
                $provider = new $className(...$providerClass['args']);
            } else {
                $className = $providerClass;
                /** @var ServiceProviderInterface $provider */
                $provider = new $className();
            }
            $provider->register($container);
        }
    }

    protected static function _getBaseProviders(): array
    {
        return [
            [
                'className' => LoggerProvider::class,
                'args' => [static::LOGGER_FILENAME],
            ],
            ConfigProvider::class,
            DbProvider::class,
        ];
    }

    abstract protected function _setupApplication(): void;
    abstract protected static function _getAdditionalProviders(): array;
}