<?php
declare(strict_types=1);

namespace App\Modules\Frontend;

use Phalcon\Di\DiInterface;
use Phalcon\Mvc\{ ModuleDefinitionInterface, View};
use App\Common\Providers\DispatcherMvcProvider;

class Module implements ModuleDefinitionInterface
{
    public function registerAutoloaders(DiInterface $container = null) {}

    public function registerServices(DiInterface $container)
    {
        $container->set('view', function() {
           $view = new View();
           $view->setViewsDir(__DIR__ . '/Views/');
           $view->registerEngines(['.volt' => 'voltService']);
           return $view;
        });

        (new DispatcherMvcProvider(__NAMESPACE__ . '\Controllers'))->register($container);
    }
}