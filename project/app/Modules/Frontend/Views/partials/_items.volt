{% if repository is defined %}
  {{ partial('partials/_pagination') }}
  {% set items = repository.getItems() %}
  {% set fieldNames = ['Код': 'id', 'Наименование': 'item', ' ': 'stock', 'Цена': 'price', 'Цена, грн': 'priceUah', 'Гар.': 'warranty', '': 'cart'] %}
  <table class="table items table-hover table-sm">
    <thead>
    <tr>
      {% for columnHeader, name in fieldNames %}
        <th scope="col">{{ columnHeader }}</th>
      {% endfor %}
    </tr>
    </thead>
    <tbody>
    {% if items|length === 0 -%}
      <tr><td colspan="5">Nothing found</td></tr>
    {% else -%}
      {% for item in items %}
        <tr data-id="{{ item['id'] }}">
          {% for name in fieldNames %}
            <td>
              {%- if name === 'cart' -%}
                <div class="input-group input-group-sm">
                  <input type="text" class="form-control quantity" value="{{ item[name] }}">
                </div>
              {% elseif name === 'stock' %}
                {% if item[name] === '1' %}
                  <i class="bi-cart4 text-primary"></i>
                {% else %}
                  <i class="bi-cart4 text-light"></i>
                {% endif %}
              {%- else -%}
                {{ item[name] }}
              {%- endif -%}
            </td>
          {% endfor %}
          {% if isDeleteShown is defined -%}
            <td>
              <a href="/cart/delete/{{ item.id }}" class="btn btn-sm btn-danger">-</a>
            </td>
          {% endif -%}
        </tr>
      {% endfor %}
    {% endif -%}
    </tbody>
  </table>
{% endif %}