{% if repository.getTotalItems() === 0 %}
  <?php return ?>
{% endif %}
{% set pageOffset = 2 %}
{% set limits = [20, 50, 100] %}
<div class="row pt-3">
  <div class="col-sm-3">
    <ul class="pagination pagination-sm mb-1">
      {% set classDisabled = repository.getCurrent() == 1 ? 'disabled' : '' %}
      <li class="page-item {{ classDisabled }}">
        {% set url = queryUrl.getUrl(['page': repository.getPrevious()]) %}
        <a class="page-link" href="{{ url }}">Prev</a>
      </li>
      {% set pageNumberMin = repository.getCurrent() - pageOffset > 0 ? repository.getCurrent() - pageOffset : 1 %}
      {% set pageNumberMax = repository.getLast() - pageOffset > repository.getCurrent() ? repository.getCurrent() + pageOffset : repository.getLast() %}
      {% for pageNumber in pageNumberMin..pageNumberMax %}
        {% set classActive = pageNumber === repository.getCurrent() ? 'active' : '' %}
        <li class="page-item {{ classActive }}">
          {% set url = queryUrl.getUrl(['page': pageNumber]) %}
          <a class="page-link" href="{{ url }}">{{ pageNumber }}</a>
        </li>
      {% endfor %}
      {% set classDisabled = repository.getCurrent() == repository.getLast() ? 'disabled' : '' %}
      <li class="page-item {{ classDisabled }}">
        {% set url = queryUrl.getUrl(['page': repository.getNext()]) %}
        <a class="page-link" href="{{ url }}">Next</a>
      </li>
    </ul>
  </div>
  <div class="col-sm-3">
    <div class="dropdown">
      <button class="btn btn-outline-primary btn-sm dropdown-toggle" data-bs-toggle="dropdown">{{ repository.getLimit() }}</button>
      <div class="dropdown-menu">
        {% for limit in limits %}
          <a class="dropdown-item" href="{{ queryUrl.remove('page').getUrl(['limit': limit]) }}">{{ limit }}</a>
        {% endfor %}
      </div>
    </div>
  </div>
</div>
