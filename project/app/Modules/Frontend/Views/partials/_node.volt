{% set catalogIdPrefix = "/" %}

<div class="list-group ms-3">
  {% for node in tree %}
    {% set nodeId = "cn" ~ node['id'] %}
    {% set href = '#' ~ nodeId %}
    {% set collapseAttributes = 'data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="' ~ nodeId ~ '"'%}
    {% if node['nodes'] is not defined %}
      {% set href = foundInFolders[node['id']] is defined ? queryUrl.remove('page').getUrl(['folder': node['id']]) : catalogIdPrefix ~ node['id'] %}
      {% set collapseAttributes = ''%}
    {% endif %}
    {% set class = '' %}
    {% if openedFolders is defined and foundInFolders is not defined and node['id'] == openedFolders[0] %}
      {% set class = ' active' %}
    {% endif %}
    {% if foundInFolders is defined and foundInFolders[node['id']] is not defined %}
      {% set class = class ~ ' d-none' %}
    {% endif %}
    <a class="list-group-item border-0 p-0{{ class }}" href="{{ href }}" {{ collapseAttributes }}>{{ node['text'] }}
      {% if foundInFolders[node['id']] is defined %}
        <span class="badge bg-light text-dark">{{ foundInFolders[node['id']] }}</span>
      {% endif %}
    </a>
    {% if node['nodes'] is defined %}
      {% set class = '' %}
      {% if openedFolders is defined and (node['id'] in openedFolders) %} {% set class=' show' %} {% endif %}
      <div class="collapse{{ class }}" id="{{ nodeId }}">
        {{ partial('partials/_node', ['tree': node['nodes']]) }}
      </div>
    {% endif %}
  {% endfor %}
</div>