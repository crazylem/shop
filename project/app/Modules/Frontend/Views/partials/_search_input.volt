{% set searchText = searchText is defined ? searchText : '' %}
{{ form(url.get(['for':'search']), 'method':'post', 'class':'form-inline') }}
  <div class="form-group mb-2 mx-sm-3">
    <div class="input-group input-group-sm">
      <input type="search" class="form-control" name="searchText" value="{{ searchText }}" placeholder="Search...">
      <button type="submit" class="btn btn-primary"><i class="bi-search"></i></button>
    </div>
  </div>

{{ end_form() }}