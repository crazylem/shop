{{ form.render(name, ['class':'form-control']) }}
{{ form.label(name) }}
{% if form.hasMessagesFor(name) %}
  <div class="alert alert-danger">
    {{ form.messages(name) }}
  </div>
{% endif %}