<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ get_title() }}</title>
  {{ assets.outputCss('headerCss') }}
</head>
<body>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12 text-center">
      {{ flashSession.output() }}
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      {{ get_content() }}
    </div>
  </div>
</div>
{{ assets.outputJs('footerJs') }}
</body>
</html>