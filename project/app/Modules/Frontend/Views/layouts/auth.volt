<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ get_title() }}</title>
  {{ assets.outputCss('headerCss') }}
</head>
<body class="text-center bg-light">
<div class="row">
  <div class="col-sm-12">
    {{ flashSession.output() }}
  </div>
</div>
{{ get_content() }}
{{ assets.outputJs('footerJs') }}
</body>
</html>