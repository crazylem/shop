<div class="row">
  <div class="col-sm-12">
    <div class="text-center">
      <h1>Oops</h1>
      <h2>404 Not Found</h2>
      <a href="/" class="btn btn-primary btn-lg">
        <i class="bi-house"></i>
        take me home
      </a>
    </div>
  </div>
</div>