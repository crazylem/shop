{% macro formSignup(form) %}
  {{ form(url.get(['for': 'signup']), 'method': 'post') }}
  {% for name in ['name', 'email', 'password', 'confirmPassword'] %}
    <div class="form-floating mb-2">
      {{ partial('partials/_formElement', ['name': name]) }}
    </div>
  {% endfor %}
  {{ partial('partials/_formElement', ['name': 'csrf']) }}
  <hr>
  {{ form.render('submit', ['class': 'btn btn-primary btn-lg w-100']) }}
  {{ end_form() }}
{% endmacro %}

<div class="form-signup m-auto">
  <h3 class="mb-3">Register</h3>
  {{ formSignup(form) }}
</div>