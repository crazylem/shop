{% macro form_signin(form) %}
  {{ form(url.get(['for': 'login']), 'method':'post') }}
  <div class="form-floating mb-2">
    {{ partial('partials/_formElement', ['name': 'email']) }}
  </div>
  <div class="form-floating mb-3">
    {{ partial('partials/_formElement', ['name': 'password']) }}
  </div>
  {{ partial('partials/_formElement', ['name': 'csrf']) }}
  {{ form.render('submit', ['class':'btn btn-primary btn-lg w-100 mb-3']) }}
  {{ end_form() }}
{% endmacro %}

<div class="form-signin m-auto">
  <h3 class="mb-3">Log in</h3>
  {{ form_signin(form) }}
  {{ link_to(url.get(['for':'signup']), 'Register', 'class':'btn btn-link') }}
</div>

