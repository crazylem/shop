<div class="row pt-2 mb-2 border-bottom">
  <div class="col-1">
    <a href="/" class="btn btn-warning btn-sm w-100">LACONIYA</a>
  </div>
  <div class="col-sm-2">
    {{ partial('partials/_search_input') }}
  </div>
  <div class="col-sm-4">
    {% block cartAndOrder %}
      <a href="/cart" class="btn btn-sm btn-primary" role="button">Корзина<span id="numberAndAmount">{{ numberAndAmount }}</span></a>
    {% endblock %}
  </div>
  <div class="col-5 text-end">
    {{ auth['name'] }}
    <a href="{{ url.get(['for':'logout']) }}" class="btn btn-sm btn-outline-primary" role="button">Log out</a>
  </div>
</div>
<div class="row">
  <div class="col-sm-3">
    <div id="tree" class="tree">
      {{ partial('partials/_node', ['tree': tree]) }}
    </div>
  </div>
  <div class="col-sm-9">
    {{ partial('partials/_items') }}
  </div>
</div>