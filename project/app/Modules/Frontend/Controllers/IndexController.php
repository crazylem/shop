<?php
declare(strict_types=1);

namespace App\Modules\Frontend\Controllers;

use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use App\Common\Components\{ QueryUrl, Utility };
use App\Common\Models\Ourprice;

/** @noinspection PhpUnused */
class IndexController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
        $this->view->setVar('numberAndAmount', Utility::getNumberAndAmount());
    }

    public function indexAction()
    {
        $this->view->setVars([
            'openedFolders' => [0],
        ]);
    }

    /** @noinspection PhpUnused */
    public function catalogAction(int $id)
    {
        $request = $this->request;

        $paginator = new PaginatorQueryBuilder([
            'builder' => (new OurPrice())->getBuilderForCatalog($id),
            'limit'   => $request->getQuery('limit', 'int', 20),
            'page'    => $request->getQuery('page', 'int', 1),
        ]);
        $repository = $paginator->paginate();

        $this->view->setVars([
            'repository' => $repository,
            'openedFolders' => Ourprice::getOpenedFolders($id),
            'queryUrl'   => new QueryUrl(),
        ]);
    }

    /** @noinspection PhpUnused
     * @noinspection PhpInconsistentReturnPointsInspection
     */
    public function searchAction()
    {
        $request = $this->request;
        $searchText = $request->getQuery('text', 'string', '');

        if ($request->isPost()) {
            $searchText = $request->getPost('searchText', ['string', 'trim'], '');
            if (strlen($searchText) === 0) {
                return $this->response->redirect('/');
            }
            $url = $this->url->get('search', [
                'text' => $searchText,
            ]);
            return $this->response->redirect($url);
        }

        $paginator = new PaginatorQueryBuilder([
            'builder' => (new OurPrice())->getBuilderForSearch([
                'searchText' => $searchText,
                'folder' => $request->getQuery('folder', 'int', 0),
            ]),
            'limit'   => $request->getQuery('limit', 'int', 20),
            'page'    => $request->getQuery('page', 'int', 1),
        ]);
        $repository = $paginator->paginate();

        $foundInFolders = (new Ourprice())->getFoundQuantityInFolders($searchText);
        $openedFolders = array_keys($foundInFolders);

        $this->view->setVars([
            'repository'     => $repository,
            'searchText'     => $searchText,
            'queryUrl'       => new QueryUrl(),
            'openedFolders'  => $openedFolders,
            'foundInFolders' => $foundInFolders,
        ]);
    }
}