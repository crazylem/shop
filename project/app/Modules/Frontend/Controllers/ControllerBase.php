<?php
declare(strict_types=1);

namespace App\Modules\Frontend\Controllers;

use Phalcon\Mvc\{ Controller, Dispatcher };
use App\Common\Components\CatalogTree;

class ControllerBase extends Controller
{
    public function initialize()
    {
        $this->assets->collection('headerCss')
            ->addCss('https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css', false)
            ->addCss('https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css', false)
            ->addCss('assets/css/frontend.css', true);

        $this->assets->collection('footerJs')
            ->addJs('assets/js/jquery-3.3.1.js')
            ->addJs('https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js', false)
            ->addJs('assets/js/frontend.js');

        $file = DIR_BASE . '/cache/tree.json';
        if (! file_exists($file)) {
            CatalogTree::createTree();
        }
        $tree = json_decode(file_get_contents(DIR_BASE . '/cache/tree.json'), true);

        $this->view->setVars([
            'tree' => $tree,
            'auth' => $this->session->get('auth')
        ]);
    }

    public function redirectSelf(): bool
    {
        $this->response->redirect($_SERVER['REQUEST_URI']);
        return false;
    }

    /** @noinspection PhpUnused */
    public function beforeExecuteRoute(Dispatcher $dispatcher): bool
    {
        if (! $this->session->has('auth') && $this->dispatcher->getControllerName() !== 'auth') {
            $this->response->redirect($this->url->get(['for' => 'login']));
            return false;
        }
        return true;
    }
}