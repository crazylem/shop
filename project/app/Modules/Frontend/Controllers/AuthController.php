<?php
declare(strict_types=1);

namespace App\Modules\Frontend\Controllers;

use Exception;
use Phalcon\Mvc\View;
use App\Common\Models\Users;
use App\Modules\Frontend\Forms\{ SignUpForm, LoginForm };

/** @noinspection LongInheritanceChainInspection */
/** @noinspection PhpUnused */
class AuthController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
        $this->assets->collection('headerCss')->addCss('assets/css/auth.css');
        $this->view->setRenderLevel(View::LEVEL_LAYOUT);
    }

    /**
     * @noinspection PhpUnused
     * @noinspection PhpInconsistentReturnPointsInspection
     */
    public function loginAction()
    {
        $form = new LoginForm();
        $this->view->setVars([
            'form' => $form,
        ]);

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if ($form->isValid($post)) {
                if (! $this->security->checkToken('csrf')) {
                    return $this->redirectSelf();
                }
                $user = Users::findFirst([
                    'email = :email:',
                    'bind' => ['email' => $post['email']],
                ]);
                if ($user && $this->security->checkHash($post['password'], $user->password)) {
                    $this->_registerSession($user);
                    $this->response->redirect('/');
                    return false;
                } else {
                    $this->flashSession->error('Email or password incorrect');
                    $this->security->hash((string) mt_rand());
                    return $this->redirectSelf();
                }

            }
        }
    }

    /** @noinspection PhpUnused */
    public function signUpAction()
    {
        $form = new SignUpForm(new Users());
        $this->view->setVars([
            'form' => $form,
        ]);

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if ($form->isValid($post)) {
                try {
                    $entity = $form->getEntity();
                    $entity->password = $this->security->hash($entity->password);
                    $entity->save();
                    $this->_registerSession($entity);
                    $this->response->redirect('/');
                } catch (Exception $e) {
                    $this->flashSession->error($e->getMessage());
                }
            }
        }
    }

    /**
     * @noinspection PhpUnused
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function logoutAction()
    {
        $this->session->remove('auth');
        $this->response->redirect('/');
        return false;
    }

    /** @noinspection PhpUnused */
    public function fourOhFourAction()
    {
    }


    protected function _registerSession(object $user)
    {
        $this->session->set('auth', [
            'id'       => $user->id,
            'name'     => $user->name,
            'category' => $user->category,
        ]);
    }
}
