<?php
declare(strict_types=1);

namespace App\Modules\Frontend\Controllers;

use Exception;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use App\Common\Components\QueryUrl;
use App\Common\Models\{ Cart, Orders };
use App\Common\Components\{ AjaxResponse, Utility };

/** @noinspection PhpUnused */
class CartController extends ControllerBase
{
    /** @noinspection PhpUnused */
    public function indexAction()
    {
        $request = $this->request;

        $paginator = new PaginatorQueryBuilder([
            'builder' => (new Cart())->getBuilder(),
            'limit'   => $request->getQuery('limit', 'int', 20),
            'page'    => $request->getQuery('page', 'int', 1),
        ]);
        $repository = $paginator->paginate();

        $this->view->setVars([
            'repository' => $repository,
            'queryUrl'   => new QueryUrl(),
            'isDeleteShown' => true,
            'numberAndAmount' => Utility::getNumberAndAmount(),
        ]);
    }

    /** @noinspection PhpUnused */
    public function deleteAction(int $itemId)
    {
        $userId = $this->session->get('auth')['id'];
        $cart = Cart::findFirst([
            'user_id = :userId: and item_id = :itemId:',
            'bind' => [
                'userId' => $userId,
                'itemId' => $itemId,
            ]]);
        $cart?->delete();
        $this->response->redirect('/cart');
    }

    /** @noinspection PhpUnused */
    public function updateAction(): bool
    {
        $request = $this->request;
        if (! $request->isAjax()) {
            return false;
        }
        $ajaxResponse = new AjaxResponse();
        if (! $request->isPost()) {
            return $ajaxResponse->fail('not post')->send();
        }
        if (($itemId = $request->getPost('itemId', 'int')) === 0) {
            return $ajaxResponse->fail('bad params')->send();
        }
        $quantity = $request->getPost('quantity', 'int');

        $cart = (new Cart())->findFirstByItemId($itemId);

        if (! $quantity) {
            try {
                $cart?->delete();
                return $ajaxResponse->success()->with([
                    'quantity' => '',
                    'numberAndAmount' => Utility::getNumberAndAmount(),
                ])->send();
            } catch (Exception $e) {
                return $ajaxResponse->fail($e->getMessage())->send();
            }
        }
        if (! $cart) {
            $cart = new Cart([
                'item_id' => $itemId,
                'user_id' => $this->session->get('auth')['id'],
            ]);
        }
        $cart->quantity = $quantity;
        try {
            $cart->save();
            return $ajaxResponse->success()->with([
                'quantity' => $quantity,
                'numberAndAmount' => Utility::getNumberAndAmount(),
            ])->send();
        } catch (Exception $e) {
            return $ajaxResponse->fail($e->getMessage())->send();
        }
    }

    /** @noinspection PhpUnused */
    public function orderAction()
    {
        try {
            (new Orders())->saveCartToOrder();
            $this->flashSession->success('Заказ сохранен');
        } catch (Exception $e) {
            $this->flashSession->error($e->getMessage());
        }
        $this->response->redirect('/cart');
    }
}