<?php
declare(strict_types=1);

namespace App\Modules\Frontend\Forms;

use Phalcon\Forms\Element\{ Text, Email, Password };
use Phalcon\Filter\Validation\Validator\{ PresenceOf, Email as ValidatorEmail, Confirmation, Alnum, Callback };
use App\Common\Forms\FormBase;
use App\Common\Models\Users;

class SignUpForm extends FormBase
{
    public function initialize()
    {
        parent::initialize();

        $name = new Text('name', ['placeholder' => 'placeholder']);
        $name->setFilters(['string', 'trim']);
        $name->addValidators([
            new PresenceOf(),
            new Alnum(),
        ]);
        $name->setLabel('Name');
        $this->add($name);

        $email = new Email('email', ['placeholder' => 'placeholder']);
        $email->setFilters('email');
        $email->addValidators([
            new PresenceOf(),
            new ValidatorEmail(),
            new Callback([
                'callback' => function($data) {
                    return ! Users::findFirst(['email = :email:',
                        'bind' => [
                            'email' => $data['email'],
                        ]]);
                },
                'message' => 'Such email already exists',
            ]),
        ]);
        $email->setLabel('Email');
        $this->add($email);

        $password = new Password('password', ['placeholder' => 'placeholder']);
        $password->addValidators([
            new PresenceOf(),
            new Confirmation([
                'message' => "Password doesn't match confirmation'",
                'with' => 'confirmPassword',
            ]),
        ]);
        $password->setLabel('Password');
        $this->add($password);

        $confirmPassword = new Password('confirmPassword', ['placeholder' => 'placeholder']);
        $confirmPassword->setLabel('Confirm password');
        $this->add($confirmPassword);
    }
}