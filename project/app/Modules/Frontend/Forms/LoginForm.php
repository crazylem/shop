<?php
declare(strict_types=1);

namespace App\Modules\Frontend\Forms;

use Phalcon\Forms\Element\{ Email, Password };
use Phalcon\Filter\Validation\Validator\{ PresenceOf, Email as ValidatorEmail };
use App\Common\Forms\FormBase;

class LoginForm extends FormBase
{
    public function initialize()
    {
        parent::initialize();

        $email = new Email('email', ['placeholder' => 'placeholder']);
        $email->setFilters('email');
        $email->addValidators([
            new PresenceOf(['cancelOnFail' => true]),
            new ValidatorEmail(),
        ]);
        $email->setLabel('Email address');
        $this->add($email);

        $password = new Password('password', ['placeholder' => 'placeholder']);
        $password->addValidators([
            new PresenceOf(),
        ]);
        $password->setLabel('Password');
        $this->add($password);
    }
}
