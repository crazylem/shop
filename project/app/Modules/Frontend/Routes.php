<?php
declare(strict_types=1);

namespace App\Modules\Frontend;

use Phalcon\Mvc\Router\Group;

class Routes extends Group
{
    public function initialize()
    {
        $this->add('/',
            [
                'controller' => 'index',
            ]);

        $this->add('/{id:(\d+)}',
            [
                'controller' => 'index',
                'action'     => 'catalog',
            ]);

        $this->add('/search',
            [
                'controller' => 'index',
                'action'     => 'search',
            ])
        ->setName('search');

        $this->add('/cart',
            [
                'controller' => 'cart',
            ]
        )->setName('cart');

        $this->add('/cart/(update|delete|order)/:params',
            [
                'controller' => 'cart',
                'action'     => 1,
                'params'     => 2,
            ]);

        $this->add('/login',
            [
                'controller' => 'auth',
                'action'     => 'login',
            ]
        )->setName('login');

        $this->add('/logout',
            [
                'controller' => 'auth',
                'action'     => 'logout',
            ]
        )->setName('logout');

        $this->add('/signup',
            [
                'controller' => 'auth',
                'action'     => 'signup',
            ]
        )->setName('signup');
    }
}