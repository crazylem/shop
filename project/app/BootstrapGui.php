<?php
declare(strict_types=1);

namespace App;

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Application;
use App\Common\Providers\{FlashProvider, RouterProvider, SessionProvider, UrlProvider, VoltProvider};

class BootstrapGui extends BootstrapBase
{
    public const PROVIDER_NAME = 'bootstrapGui';

    protected const LOGGER_FILENAME = 'gui';

    protected function _setupApplication(): void
    {
        $container = new FactoryDefault();
        $container->setShared(self::PROVIDER_NAME, $this);
        $this->_app = new Application($container);
    }

    public function initialize(): void
    {
        parent::initialize();
        $this->_app->registerModules([
            'Frontend' => [
                'className' => Modules\Frontend\Module::class,
                'routes' => Modules\Frontend\Routes::class,
            ],
        ]);
    }

    protected static function _getAdditionalProviders(): array
    {
        return [
            SessionProvider::class,
            FlashProvider::class,
            RouterProvider::class,
            UrlProvider::class,
            VoltProvider::class,
        ];
    }


}