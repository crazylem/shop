<?php
declare(strict_types=1);

namespace App;

use Phalcon\Cli\Console;
use Phalcon\Di\FactoryDefault\Cli as CliDI;
use App\Common\Providers\{DispatcherCliProvider, RedisProvider};

class BootstrapCli extends BootstrapBase
{
    protected const LOGGER_FILENAME = 'cli';

    protected function _setupApplication(): void
    {
        $this->_app = new Console(new CliDI());
    }

    protected static function _getAdditionalProviders(): array
    {
        return [
            RedisProvider::class,
            [
                'className' => DispatcherCliProvider::class,
                'args' => [__NAMESPACE__ . '\Tasks'],
            ],
        ];
    }
}