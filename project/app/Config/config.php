<?php
declare(strict_types=1);

return [
    'debug' => $_ENV['DEBUG'],
    'database' => [
        'adapter'  => $_ENV['DB_ADAPTER'],
        'options' => [
            'host'     => $_ENV['DB_HOST'],
            'username' => $_ENV['DB_USERNAME'],
            'password' => $_ENV['DB_PASSWORD'],
            'dbname'   => $_ENV['DB_DBNAME'],
        ],
    ],
    'api' => [
        'auth' => [
            'key' => $_ENV['API_AUTH_KEY'],
            'value' => $_ENV['API_AUTH_VALUE'],
            'ignore' => $_ENV['API_AUTH_IGNORE'],
        ],
    ],
    'redis' => [
        'host' => $_ENV['REDIS_HOST'],
    ],
];
