<?php
declare(strict_types=1);

namespace App;

use Exception;
use Phalcon\Di\FactoryDefault;
use Phalcon\Logger\Logger;
use Phalcon\Mvc\Micro;
use App\Common\Components\{CatalogTree, SearchInet\SearchInet};
use App\Common\Models\{Ourprice, UsdRate, Orders};
use App\Common\Providers\{RedisProvider, SessionProvider};

class BootstrapApi extends BootstrapBase
{
    protected const LOGGER_FILENAME = 'api';

    public function initialize(): void
    {
        parent::initialize();

        $app = $this->_app;
        $container = $app->getDI();

        /** @var Logger $logger */
        $logger = $container->get('logger');
        $requestBody = rtrim(substr($container->get('request')->getRawBody(), 3), "\r\n");

        $app->before(function () use ($app, $container) {
            $config = $container->get('config');
            $auth = $config->api->auth;

            if ($config->debug == 1 && $auth->ignore == 1) {
                return;
            }

            $request = $app->request;
            $headers = $request->getHeaders();
            if (array_key_exists($auth->key, $headers) && $headers[$auth->key] === $auth->value) {
                return;
            }

            $app->response->setStatusCode(401, 'Unauthorized');
            echo "error\nerror 401";
            $app->stop();
        });

        //TODO: test
        $app->get('/api/test', function () use ($container, $requestBody) {
//            echo 'this is api test';
            CatalogTree::createTree();
        });
        //TODO: end of test

        $app->get('/api/orders', function () {
            echo Orders::getNotUploaded();
        });

        $app->put('/api/orders/uploaded/{id}', function ($id) use ($requestBody) {
            Orders::setUploaded($id, $requestBody);
        });

        $app->put('/api/item', function () use ($requestBody, $logger) {
            $id = Ourprice::updateFromTsv($requestBody);
            $logger->info('Updated item with id = ' . $id);
        });

        $app->put('/api/tree', function () use ($logger) {
            CatalogTree::createTree();
            $logger->info('Updated tree');
        });

        $app->put('/api/usd-rate', function () use ($requestBody, $logger) {
            UsdRate::updateRate($requestBody);
            $logger->info('Updated usd exchange rate');
        });

        $app->get('/api/search-inet/price/{price}/{extension}', function ($price, $extension) use ($app) {
            $filename = sprintf('%s%s.%s', SearchInet::PRICE_PATH, ucfirst($price), $extension);
            echo file_get_contents($filename);

    });
        $app->put('/api/search-inet/update', function () use ($container) {
            (new SearchInet($container))->update();
        });

        $app->notFound(function () {
            throw new Exception('not found');
        });
    }

    protected function _setupApplication(): void
    {
        $container = new FactoryDefault();
        $app = new Micro($container);
        $this->_app = $app;
    }

    protected static function _getAdditionalProviders(): array
    {
        return [
            SessionProvider::class,
            RedisProvider::class,
        ];
    }
}