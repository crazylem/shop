<?php
declare(strict_types=1);

namespace App\Common\Forms;

use Phalcon\Encryption\Security\Exception;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\{ Hidden, Submit };
use Phalcon\Filter\Validation\Validator\Identical;

class FormBase extends Form
{
    public function messages(string $name): string
    {
        $messages = [];
        foreach($this->getMessagesFor($name) as $message) {
            $messages[] = $message;
        }
        return implode('<br>', $messages);
    }

    /**
     * @throws Exception
     */
    public function initialize()
    {
        $submit = new Submit('submit');
        $this->add($submit);

        $token = $this->security->getSessionToken() ?? $this->security->getToken();
        $csrf = new Hidden('csrf', ['value' => $token]);
        $csrf->addValidator(new Identical([
            'value' => $this->security->getSessionToken(),
            'message' => 'Session expired',
        ]));
        $this->add($csrf);
    }

    public function label(string $name, array $attributes = null): string
    {
        $element = $this->get($name);
        if (! is_a($element, Hidden::class, true)) {
            return parent::label($name, $attributes);
        }
        return '';
    }
}