<?php
declare(strict_types=1);

namespace App\Common\Providers;

use Phalcon\Config\Config;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class ConfigProvider implements ServiceProviderInterface
{
    protected string $_providerName = 'config';

    public function register(DiInterface $di): void
    {
        $di->setShared($this->_providerName, function() {
            $config = include DIR_BASE . '/app/Config/config.php';
            return new Config($config);
        });
    }
}