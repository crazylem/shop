<?php
declare(strict_types=1);

namespace App\Common\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Logger\Logger;
use Phalcon\Logger\Formatter\Line as FormatterLine;
use Phalcon\Logger\Adapter\Stream;

class LoggerProvider implements ServiceProviderInterface
{
    public function __construct(protected string $_loggerFilename = 'main')
    {
    }

    public function register(DiInterface $di): void
    {
        $filename = $this->_loggerFilename;
        $di->set('logger', function() use ($filename) {
            $adapter = new Stream(sprintf('%s/cache/logs/%s.log', DIR_BASE, $filename));
            $adapter->setFormatter(self::_getFormatter());
            return new Logger('messages', [$adapter]);
        });

        $di->set('loggerCrawler', function() {
            $adapter = new Stream(DIR_BASE . '/cache/logs/crawler.log');
            $adapter->setFormatter(self::_getFormatter());
            return new Logger('messages', [$adapter]);
        });
    }

    protected static function _getFormatter(): FormatterLine
    {
        $formatter = new FormatterLine('[%date%] - [%level%] - [%message%]');
        $formatter->setDateFormat('Y-m-d H:i:s');
        return $formatter;
    }
}