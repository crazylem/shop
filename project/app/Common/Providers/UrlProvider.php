<?php
declare(strict_types=1);

namespace App\Common\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Mvc\Url;

class UrlProvider implements ServiceProviderInterface
{
    public function register(DiInterface $di): void
    {
        $di->set('url', function() {
            $url = new Url();
            $url->setBaseUri('/');
            return $url;
        });
    }
}