<?php
declare(strict_types=1);

namespace App\Common\Providers;

use Phalcon\Di\{ DiInterface, ServiceProviderInterface };
use Phalcon\Mvc\Dispatcher;

class DispatcherMvcProvider implements ServiceProviderInterface
{
    public const PROVIDER_NAME = 'dispatcher';

    public function __construct(protected string $_defaultNamespace)
    {
    }

    public function register(DiInterface $di): void
    {
        $defaultNamespace = $this->_defaultNamespace;
        $di->set(self::PROVIDER_NAME, function() use ($defaultNamespace) {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace($defaultNamespace);
            return $dispatcher;
        });
    }
}