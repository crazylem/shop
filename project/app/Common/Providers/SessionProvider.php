<?php
declare(strict_types=1);

namespace App\Common\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Session\Manager;
use Phalcon\Session\Adapter\Stream;

class SessionProvider implements ServiceProviderInterface
{
    public const PROVIDER_NAME = 'session';

    public function register(DiInterface $di): void
    {
        $di->set(self::PROVIDER_NAME, function() {
            $session = new Manager();
            $files = new Stream(['savePath' => DIR_BASE . '/cache/session/']);
            $session
                ->setAdapter($files)
                ->start();
            return $session;
        });
    }
}