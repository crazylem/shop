<?php
declare(strict_types=1);

namespace App\Common\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Mvc\ViewBaseInterface;
use Phalcon\Mvc\View\Engine\Volt;

class VoltProvider implements ServiceProviderInterface
{
    public function register(DiInterface $di): void
    {
        $di->setShared(
            'voltService',
            function (ViewBaseInterface $view) use ($di) {
                $volt = new Volt($view, $di);
                $volt->setOptions([
                    'always' => true,
                    'extension' => '.php',
                    'separator' => '_',
                    'stat' => true,
                    'path' => DIR_BASE . '/cache/volt/',
                    'prefix' => '',
                ]);
                return $volt;
            });
    }
}