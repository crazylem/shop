<?php
declare(strict_types=1);

namespace App\Common\Providers;

use Phalcon\Db\Adapter\PdoFactory;
use Phalcon\Di\{ DiInterface, FactoryDefault };
use Phalcon\Di\ServiceProviderInterface;

class DbProvider implements ServiceProviderInterface
{
    protected const PROVIDER_NAME = 'db';

    public function register(DiInterface $di): void
    {
        $di->set(self::PROVIDER_NAME, function() {
            /** @var FactoryDefault $this */
            return (new PdoFactory())->load($this->getShared('config')->database);
        });
    }
}