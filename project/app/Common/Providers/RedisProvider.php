<?php
declare(strict_types=1);

namespace App\Common\Providers;

use Redis;
use Phalcon\Di\{DiInterface, FactoryDefault, ServiceProviderInterface};

class RedisProvider implements ServiceProviderInterface
{
    protected const PROVIDER_NAME = 'redis';

    public function register(DiInterface $di): void
    {
        $di->setShared(self::PROVIDER_NAME, function() {
            $redis = new Redis();
            /** @var FactoryDefault $this */
            $redis->connect($this->getShared('config')->redis->host);
            return $redis;
        });
    }
}