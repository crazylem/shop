<?php
declare(strict_types=1);

namespace App\Common\Providers;

use Phalcon\Di\{ DiInterface, ServiceProviderInterface };
use Phalcon\Mvc\{ Application, Router };
use App\BootstrapGui;

class RouterProvider implements ServiceProviderInterface
{
    public const PROVIDER_NAME = 'router';

    public const DEFAULT_MODULE = 'Frontend';

    public function register(DiInterface $di): void
    {
        /** @var Application $app */
        $app = $di->getShared(BootstrapGui::PROVIDER_NAME)->getApplication();
        $di->set(self::PROVIDER_NAME, function() use ($app) {
            $router = new Router(false);
            $router->notFound([
                'controller' => 'auth',
                'action'     => 'fourOhFour',
            ]);
            $router->setDefaultAction('index');
            $router->setDefaultModule(self::DEFAULT_MODULE);
            foreach ($app->getModules() as $module) {
                $router->mount(new $module['routes']());
            }
            return $router;
        });
    }
}