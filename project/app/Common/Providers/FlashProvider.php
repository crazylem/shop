<?php
declare(strict_types=1);

namespace App\Common\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Html\Escaper;
use Phalcon\Flash\Session as FlashSession;

class FlashProvider implements ServiceProviderInterface
{
    public const PROVIDER_NAME = 'flashSession';

    public function register(DiInterface $di): void
    {
        $escaper = new Escaper();
        $di->set(self::PROVIDER_NAME, function() use ($escaper) {
            $flash = new FlashSession($escaper);
            $flash->setCssClasses([
                'error'   => 'alert alert-danger',
                'success' => 'alert alert-success',
                'notice'  => 'alert alert-info',
                'warning' => 'alert alert-warning',
            ]);
            return $flash;
        });
    }
}