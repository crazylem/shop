<?php
declare(strict_types=1);

namespace App\Common\Components;

use DOMDocument, DOMXPath, Exception;
use App\Common\Models\{ Cart, UsdRate };

class Utility
{
    public static function getNumberAndAmount(): string
    {
        $numberAndAmount = (new Cart())->getNumberAndAmount();
        return sprintf(' на %.2f$ (%.2fгрн) в %d поз.',
            $numberAndAmount['amount'],
            $numberAndAmount['amount'] * UsdRate::getLast(),
            $numberAndAmount['number']);
    }

    /**
     * @throws Exception
     */
    public static function getCurlHtml(string $url, array $customOpts = []): string
    {
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
        ];
        $options = array_replace($options, $customOpts);
        $c = curl_init();
        curl_setopt_array($c, $options);
        if (! $html = curl_exec($c)) {
            throw new Exception('curl error. ' . curl_error($c));
        }
        curl_close($c);
        return $html;
    }

    /**
     * @throws Exception
     */
    public static function getXpath(string $url, array $curlOpts = []): DOMXPath
    {
        $html = self::getCurlHtml($url, $curlOpts);
        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
        libxml_use_internal_errors(false);
        return new DOMXpath($doc);
    }
}