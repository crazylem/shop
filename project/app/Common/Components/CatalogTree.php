<?php
declare(strict_types=1);

namespace App\Common\Components;

use App\Common\Models\Ourprice;

class CatalogTree
{
    public static function createTree(): void
    {
        file_put_contents(
            DIR_BASE . '/cache/tree.json',
            json_encode(self::_getNested(0), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
    }

    protected static function _getNested(int $pid): array
    {
        $out = [];
        foreach (Ourprice::findFoldersByPid($pid) as $entity) {
            $node = [];
            $node['text'] = $entity->folderName;
            $node['id'] = $entity->id;
            $nested = self::_getNested($entity->id);
            if (count($nested)) {
                $node['nodes'] = $nested;
            }
            $out[] = $node;
        }
        return $out;
    }
}