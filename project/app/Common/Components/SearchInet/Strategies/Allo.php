<?php
declare(strict_types=1);

namespace App\Common\Components\SearchInet\Strategies;

use DOMNode;
use DOMNodeList;
use Exception;

/** @noinspection PhpUnused */
class Allo extends AbstractStrategy
{
    use XpathFetchingTrait;

    /**
     * @throws Exception
     */
    public function getData(): array|bool
    {
        return $this->getDataByXpath();
    }

    protected function _getSearchUrl(): string
    {
        return 'https://allo.ua/ru/catalogsearch/result/index/seller-allo/?q=';
    }

    protected function _getNodeList(): DOMNodeList
    {
        return $this->_xpath->query('//*[@class="products-layout__container products-layout--grid"]/div');
    }

    protected function _getName(DOMNode $node): string
    {
        return $this->_xpath->query('.//*[@class="product-card__title"]', $node)[0]->textContent;
    }

    protected function _getCode(DOMNode $node): string
    {
        return $this->_xpath->query('.//*[@class="product-sku__value"]', $node)[0]->textContent;
    }

    protected function _getPrice(DOMNode $node): string
    {
        $nodeList = $this->_xpath->query('.//*[@class="sum"]', $node);
        if (count($nodeList) === 0) {
            return '0';
        }
        return mb_ereg_replace(' ', '', $nodeList[0]->textContent);
    }

    protected function _getStock(DOMNode $node): string
    {
        return count($this->_xpath->query('.//*[@class="sum"]', $node)) > 0 ? 'Есть' : 'Нет в наличии';
    }

    protected function _getWarranty(DOMNode $node): string
    {
        return '0';
    }

    protected function _checkNode(DOMNode $node): bool
    {
        $content = $this->_xpath->query('.//*[@class="product-card__detail"]/dl/dd', $node)[0]->textContent;
        return mb_stripos($content, 'уценка') === false;
    }
}