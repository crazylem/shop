<?php
declare(strict_types=1);

namespace App\Common\Components\SearchInet\Strategies;

use App\Common\Models\Ourprice;

abstract class AbstractStrategy
{
    protected const KEYWORDS_COUNT = 3;

    /**
     * @return array|bool
     */
    abstract public function getData(): array|bool;

    protected string $_message;

    public function __construct(protected Ourprice $_item)
    {
    }

    final public function getMessage(): string
    {
        return $this->_message;
    }

    protected function _fail(string $message): bool
    {
        $this->_message = sprintf('%s. (%s)%s',$message , $this->_item->id, $this->_item->item);
        return false;
    }

    protected function _addKeywords(string $baseUrl): string
    {
        $out = $baseUrl . urlencode($this->_item->keyword1);
        $out .= $this->_item->keyword2 === '' ? '' : '+' . urlencode($this->_item->keyword2);
        $out .= $this->_item->keyword3 === '' ? '' : '+' . urlencode($this->_item->keyword3);
        return $out;
    }

    protected function _checkKeywords(string $name): bool
    {
        for ($i = 1; $i <= self::KEYWORDS_COUNT; $i++) {
            $propertyName = 'keyword' . $i;
            if ($this->_item->{$propertyName} !== '' && mb_stripos($name, $this->_item->{$propertyName}) === false) {
                return false;
            }
        }
        return $this->_item->keyword_not === '' || mb_stripos($name, $this->_item->keyword_not) === false;
    }
}