<?php
declare(strict_types=1);

namespace App\Common\Components\SearchInet\Strategies;

use DOMNode, DOMNodeList, DOMXPath, Exception;
use App\Common\Components\Utility;

trait XpathFetchingTrait
{
    protected DOMXPath $_xpath;

    /**
     * @throws Exception
     */
    public function getDataByXpath(): array|bool
    {
        $url = $this->_addKeywords(self::_getSearchUrl());
        $this->_xpath = Utility::getXpath($url, $this->_getCurlOptions());

        if (! $nodeList = $this->_getNodeList()) {
            return $this->_fail('invalid xpath expression');
        }

        if (count($nodeList) === 0) {
            return $this->_fail('nothing found');
        }

        $isMatchFound = false;
        foreach ($nodeList as $node) {
            $name = $this->_getName($node);
            if (! ($this->_checkKeywords($name) && $this->_checkNode($node))) {
                continue;
            }
            if ($isMatchFound) {
                return $this->_fail('too many found');
            }
            $isMatchFound = true;
            $out = [
                'code' => $this->_getCode($node),
                'name' => $name,
                'price' => $this->_getPrice($node),
                'stock' => $this->_getStock($node),
                'warranty' => $this->_getWarranty($node),
            ];
        }

        if (! isset($out)) {
            return $this->_fail('nothing found');
        }

        return $out;
    }

    protected function _checkNode(DOMNode $node): bool
    {
        return true;
    }

    protected function _getCurlOptions(): array
    {
        return [];
    }

    abstract protected function _getSearchUrl(): string;
    abstract protected function _getNodeList(): DOMNodeList;
    abstract protected function _getName(DOMNode $node): string;
    abstract protected function _getCode(DOMNode $node): string;
    abstract protected function _getPrice(DOMNode $node): string;
    abstract protected function _getStock(DOMNode $node): string;
    abstract protected function _getWarranty(DOMNode $node): string;
}