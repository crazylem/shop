<?php
declare(strict_types=1);

namespace App\Common\Components\SearchInet\Strategies;

use Exception;
use App\Common\Components\Utility;

/** @noinspection PhpUnused */
class Rozetka extends AbstractStrategy
{
    /**
     * @throws Exception
     */
    public function getData(): array|bool
    {
        $url = $this->_addKeywords('https://search.rozetka.com.ua/search/api/v6/?front-type=xl&country=UA&lang=ru&seller=rozetka&text=');
        $foundItems = json_decode(Utility::getCurlHtml($url))->data->goods;

        if (count($foundItems) === 0) {
            return $this->_fail('nothing found');
        }

        $categoryId = $foundItems[0]->category_id;
        $isMatchFound = false;
        foreach ($foundItems as $foundItem) {
            if ($foundItem->category_id !== $categoryId || ! $this->_checkKeywords($foundItem->title)) {
                continue;
            }
            if ($isMatchFound) {
                return $this->_fail('too many found');
            }
            $isMatchFound = true;
            $out = [
                'code' => $foundItem->id,
                'name' => $foundItem->title,
                'price' => $foundItem->price,
                'stock' => $foundItem->sell_status,
            ];
        }

        if (! isset($out)) {
            return $this->_fail('nothing found');
        }

        $url = 'https://product-api.rozetka.com.ua/v4/goods/get-guarantee?front-type=xl&country=UA&lang=ru&goodsId=' . $out['code'];
        $warranty = json_decode(Utility::getCurlHtml($url))->data->firstBlock;
        $out['warranty'] = substr($warranty, 0, strpos($warranty, ' '));

        return $out;
    }
}