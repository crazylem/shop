<?php
declare(strict_types=1);

namespace App\Common\Components\SearchInet\Strategies;

use Exception;
use App\Common\Components\Utility;

/** @noinspection PhpUnused */
class Eldorado extends AbstractStrategy
{
    /**
     * @return array|bool
     * @throws Exception
     */
    public function getData(): array|bool
    {
        $url = $this->_addKeywords('https://api.eldorado.ua/v1/new_search?limit=20&q=');

        if (is_null($json = json_decode(Utility::getCurlHtml($url, self::_getCurlOptions())))) {
            return $this->_fail('curl error');
        }
        if (property_exists($json, 'error')) {
            return $this->_fail('nothing found');
        }

        $isMatchFound = false;
        foreach($json->data->collection as $foundItem) {
            if (! $this->_checkKeywords($foundItem->title)) {
                continue;
            }
            if ($isMatchFound) {
                return $this->_fail('too many found');
            }
            $isMatchFound = true;
            $out = [
                'code' => $foundItem->id,
                'name' => $foundItem->title,
                'price' => $foundItem->price,
                'stock' => self::_getSellStatusText($foundItem->sell_status),
                'warranty' => 0,
            ];
        }

        if (! isset($out)) {
            return $this->_fail('nothing found');
        }

        return $out;
    }

    protected function _getSellStatusText(string $code): string
    {
        return match ($code) {
            '0' => 'продано',
            '2' => 'Последний товар',
            '3' => 'Заканчивается',
            '4' => 'В наличии',
            default => 'Unknown sell status',
        };
  }

    /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */
    protected static function _getCurlOptions(): array
    {
        return [
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36',
            CURLOPT_COOKIE => 'sc=6B5D5C98-03A6-3F7E-FEEF-091DFDE4171E; _ga=GA1.1.1826364120.1607659443; _hjSessionUser_755824=eyJpZCI6ImUwZjc2ODZkLTRmOTMtNTYwMS04YzgwLTUwN2UwNzcwOGQ0OCIsImNyZWF0ZWQiOjE2NTA1OTA2ODA2MDcsImV4aXN0aW5nIjpmYWxzZX0=; _ga_P30Z0HYN06=GS1.1.1650590679.1.0.1650590685.54; eldorado=12f34342575aa48c0ba9e9130516b4b2; incap_ses_245_1842303=dXQpX/w42Am7gHhi4mpmAw0v72IAAAAAH+zSH+W95p40GiVqpBNFkg==; visid_incap_1842303=0bLcBkqOQ02MSk4QjkcpRdUDYmIAAAAAQkIPAAAAAACAph6mAbyjHx5rqK5GIy4aIruEAp2SAW5L',
        ];
    }
}