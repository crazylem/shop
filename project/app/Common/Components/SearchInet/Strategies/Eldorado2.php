<?php
declare(strict_types=1);

namespace App\Common\Components\SearchInet\Strategies;

use Exception, DOMNode, DOMNodeList;
use App\Common\Components\Utility;

/** @noinspection PhpUnused */
class Eldorado2 extends AbstractStrategy
{
    use XpathFetchingTrait;

    /**
     * @return array|bool
     * @throws Exception
     */
    public function getData(): array|bool
    {
        return $this->getDataByXpath();
    }

    protected function _getSearchUrl(): string
    {
        return 'https://eldorado.ua/search/?q=';
    }

    /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */
    protected function _getCurlOptions(): array
    {
        return [
            CURLOPT_COOKIE => 'userGUID=200817d0-cc3f-11ec-829a-fd5bc42714e5; is_search=1; ModalLeftProductsStatus=available; sc=7B28CFB4-1FCF-48E8-8C33-BC78AA9D038F; rngst2={"utmz":{"utm_source":"google","utm_medium":"cpc","utm_campaign":"Promodo_ukr_Shop_Smart_MDA_Холодильники","utm_content":"cid|1709199019|holodilniki","utm_term":"(none)"}}; ABtesting=not_set; incap_ses_877_1842303=ZuR3awc1lB8Bw7AqdLsrDNVyc2IAAAAAxFWbiHtb5Cue7mYVNrE3Jw==; _gcl_au=1.1.680415336.1651733204; thxPage=undefined; displaytrue=false; _gid=GA1.2.733503978.1651733205; _hjSessionUser_755824=eyJpZCI6IjcyM2U0ZTEyLWQwODktNWMzZi05MGQ0LThkYjQ1MTNiYTY2MyIsImNyZWF0ZWQiOjE2NDAxMTc3NDY1NDgsImV4aXN0aW5nIjp0cnVlfQ==; _hjSession_755824=eyJpZCI6IjJmYjEzNTVmLTUyYjEtNGZiMi04MDYyLWQ2ZGI4ZTA1NzY5NyIsImNyZWF0ZWQiOjE2NTE3MzMyMDYzNjYsImluU2FtcGxlIjp0cnVlfQ==; _hjIncludedInSessionSample=1; _hjAbsoluteSessionInProgress=0; rngst_callback={"callbackNumber":false,"inactive_project":false,"ip_is_blocked":false,"recaptcha":0}; rngst1={"checkOnClient":[0,1,2,5,6,7,8,9,10,11,12,14,15,16,17,18,20,21,22,23,25,26,27,28,29,30,31,32,33,34,35,36],"numbers":true,"":[13,19,24,45],"380442992154":[37,40,4],"380800217637":[38,39,41,42,43,44,46,47,3]}; _fbp=fb.1.1651733226237.1545439624; userId=not set; incap_ses_876_1842303=46gsGQT2Eg/xpUOF9S0oDAd2c2IAAAAAPPnp2ilJFUfA65YtB7jxCg==; visid_incap_1842303=xhBhi0zRQeqTJvbFEbnezOs1wmEAAAAAQkIPAAAAAACAxA6kAbyjHx5Nq0pwctLpNcpL2X9LEbj+; eldorado=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJndWVzdCIsInN1YiI6InNlc3Npb25JZF8xYjZlODQ2OC0wZGM0LTRiZjctYjUzYS02MDc2NTU2NDUzNTQiLCJpYXQiOjE2NTE3MzM2ODksImV4cCI6MTY1Mjk0MzYyOX0.MKRfJXWyYYjYh0wSqDpx7UbF3TlgmmfDWgM98v2bHuw; fp=18; lfp=5/5/2022, 9:46:44 AM; _ga_P30Z0HYN06=GS1.1.1651733203.2.1.1651734026.54; _ga=GA1.2.1997730288.1640117742; _gat_UA-87413944-1=1; pa=1651733510609.75660.7679393736941031eldorado.ua0.7571266614561933+2',
        ];
    }


    protected function _getNodeList(): DOMNodeList
    {
        return $this->_xpath->query('//*[@class="goods-item-content"]');
    }

    protected function _getName(DOMNode $node): string
    {
        return $this->_xpath->query('.//*[@class="title-text"]', $node)[0]->textContent;
    }

    protected function _getCode(DOMNode $node): string
    {
        $content = $this->_xpath->query('.//*[@class="goods-code"]', $node)[0]->textContent;
        return substr($content, 9);
    }

    protected function _getPrice(DOMNode $node): string
    {
        return $this->_xpath->query('.//*[@class="price-place "]', $node)[0]->nodeValue;
    }

    /**
     * @param DOMNode $node
     * @return string
     * @throws Exception
     */
    protected function _getStock(DOMNode $node): string
    {
        $href = $this->_xpath
            ->query('.//*[@class="image-place"]//a', $node)[0]
            ->getAttribute('href');
        return Utility::getXpath('eldorado.ua' . $href)
            ->query('//*[@class="product-status"]')[0]
            ->textContent;
    }

    protected function _getWarranty(DOMNode $node): string
    {
        return '0';
    }
}