<?php
declare(strict_types=1);

namespace App\Common\Components\SearchInet\Strategies;

use DOMNode;
use DOMNodeList;
use Exception;

/** @noinspection PhpUnused */
class Foxtrot extends AbstractStrategy
{
    use XpathFetchingTrait;

    /**
     * @throws Exception
     */
    public function getData(): array|bool
    {
        return $this->getDataByXpath();
    }

    protected function _getSearchUrl(): string
    {
        return 'https://www.foxtrot.com.ua/ru/search?query=';
    }

    protected function _getNodeList(): DOMNodeList
    {
        return $this->_xpath->query('//*[@class="listing__body-wrap image-switch"]/section/article');
    }

    protected function _getName(DOMNode $node): string
    {
        return $this->_xpath->query('.//*[@class="card__title"]', $node)[0]->textContent;
    }

    protected function _getCode(DOMNode $node): string
    {
        $content = $this->_xpath->query('.//*[@class="card__code"]', $node)[0]->textContent;
        return mb_ereg_replace('[\n\ Код:\.]', '', $content);
    }

    protected function _getPrice(DOMNode $node): string
    {
        $content = $this->_xpath->query('.//*[@class="card-price"]', $node)[0]->textContent;
        return mb_ereg_replace('[\n\ ₴]', '', $content);
    }

    protected function _getStock(DOMNode $node): string
    {
        return 'в наличии';
    }

    protected function _getWarranty(DOMNode $node): string
    {
        return '0';
    }
}