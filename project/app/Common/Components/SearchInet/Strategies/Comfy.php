<?php
declare(strict_types=1);

namespace App\Common\Components\SearchInet\Strategies;

use DOMNode, DOMNodeList, Exception;

/** @noinspection PhpUnused */
class Comfy extends AbstractStrategy
{
    use XpathFetchingTrait;

    /**
     * @return array|bool
     * @throws Exception
     */
    public function getData(): array|bool
    {
        return $this->getDataByXpath();
    }

    protected function _getSearchUrl(): string
    {
        return 'https://comfy.ua/search/?q=';
    }

    protected function _getNodeList(): DOMNodeList
    {
        return $this->_xpath->query('//*[@class="products-catalog"]/div');
    }

    protected function _getName(DOMNode $node): string
    {
        return $this->_xpath->query('.//*[@class="products-list-item__name"]', $node)[0]->textContent;
    }

    protected function _getCode(DOMNode $node): string
    {
        $content =  $this->_xpath->query('.//*[@class="products-list-item__code dsk"]', $node)[0]->textContent;
        return mb_ereg_replace('[\n\ Код:\.]', '', $content);
    }

    protected function _getPrice(DOMNode $node): string
    {
        $content = $this->_xpath->query('.//*[@class="products-list-item__actions-price-current"]', $node)[0]->textContent;
        return mb_ereg_replace('[\n\ ₴]', '', $content);
    }

    protected function _getStock(DOMNode $node): string
    {
        $nodeList = $this->_xpath->query('.//*[@class="products-list-item__annotation-title products-list-item__annotation-out-of-stock"]', $node);
        if (count($nodeList) === 0) {
            return 'В наличии';
        }
        return mb_ereg_replace('(^[\n\ ]*|[\n\ ]*$)', '', $nodeList[0]->textContent);
    }

    protected function _getWarranty(DOMNode $node): string
    {
        return '0';
    }
}