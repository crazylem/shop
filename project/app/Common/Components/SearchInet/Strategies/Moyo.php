<?php
declare(strict_types=1);

namespace App\Common\Components\SearchInet\Strategies;

use DOMNode, DOMNodeList, Exception;

/** @noinspection PhpUnused */
class Moyo extends AbstractStrategy
{
    use XpathFetchingTrait;

    /**
     * @throws Exception
     */
    public function getData(): array|bool
    {
        return $this->getDataByXpath();
    }

    protected function _getSearchUrl(): string
    {
        return 'https://www.moyo.ua/search/new/?q=';
    }

    protected function _getNodeList(): DOMNodeList
    {
        return $this->_xpath->query('//*[@class="search_products js-products-list"]/div');
    }

    protected function _getName(DOMNode $node): string
    {
        $content = $this->_xpath->query('.//*[@class="product-item_name gtm-link-product"]', $node)[0]->textContent;
        return mb_ereg_replace('[\n\t]', '', $content);
    }

    protected function _getCode(DOMNode $node): string
    {
        $content = $this->_xpath->query('.//*[@class="product-item_id"]', $node)[0]->textContent;
        return mb_ereg_replace('Код товара: ', '', $content);
    }

    protected function _getPrice(DOMNode $node): string
    {
        $content = $this->_xpath->query('.//div[contains(@class, "product-item_price_current")]', $node)[0]->textContent;
        return mb_ereg_replace("[\n\  грн]", '', $content);
    }

    protected function _getStock(DOMNode $node): string
    {
        $nodeList =  $this->_xpath->query('.//*[@class="product-item_price_current not-available"]', $node);
        return count($nodeList) === 0 ? 'В наличии/Заканчивается' : 'Нет в наличии';
    }

    protected function _getWarranty(DOMNode $node): string
    {
        return '0';
    }

    protected function _checkNode(DOMNode $node): bool
    {
        $nodeList =  $this->_xpath->query('.//*[@class="product-item_info_markdown_title"]', $node);
        return count($nodeList) === 0;
    }
}