<?php
declare(strict_types=1);

namespace App\Common\Components\SearchInet;

use Exception;
use Phalcon\Di\DiInterface;
use Phalcon\Logger\Logger;
use Phalcon\Mvc\Model\ResultsetInterface;
use PhpOffice\PhpSpreadsheet\{IOFactory, Spreadsheet, Worksheet\Worksheet};
use App\Common\Components\SearchInet\Strategies\AbstractStrategy;
use App\Common\Models\{Ourprice, SearchInetFound, SearchInetSite, UsdRate};


class SearchInet
{
    public const PRICE_PATH = DIR_BASE . '/cache/SearchInet/';
    private const REDIS_KEY_ALREADY_RUNNING = 'SearchInetIsAlreadyRunning';

    protected ResultsetInterface $_sites;

    protected float $_usdRate;

    public function __construct(
        protected DiInterface $_container
    ) {
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws Exception
     */
    public function update(): void
    {
        $container = $this->_container;
        if ($this->isRunning()) {
            throw new Exception('SearchInet update is already running');
        }
        /** @var Logger $logger */
        $logger = $container->get('loggerCrawler');
        $start = microtime(true);
        $redis = $container->getShared('redis');
        $redis->set(self::REDIS_KEY_ALREADY_RUNNING, 1);

        try {
            SearchInetFound::deleteAll();
            $items = Ourprice::findBySearchInet(1);
            $this->_sites = SearchInetSite::findByActive(1);
            $this->_usdRate = UsdRate::getLast();

            foreach ($items as $item) {
                foreach ($this->_sites as $site) {
                    $strategy = self::_getStrategy($site->name, $item);
                    try {
                        if (false === $data = $strategy->getData()) {
                            $data = [
                                'code' => 'error',
                                'name' => $strategy->getMessage(),
                            ];
                        } else {
                            $data['price'] /= $this->_usdRate;
                        }
                        $data['item_id'] = $item->id;
                        $data['search_inet_site_id'] = $site->id;
                        SearchInetFound::createRecord($data);
                    } catch (Exception $e) {
                        $logger->warning(sprintf('%s. Site %s. (%d)%s',
                            $e->getMessage(),
                            $site->name,
                            $item->id,
                            $item->item));
                    }
                }
                sleep(1);
            }
            $this->_saveSites();
        } finally {
            $redis->del(self::REDIS_KEY_ALREADY_RUNNING);
        }
        $logger->info(sprintf('Update %d items takes %d seconds',
            count($items) * count($this->_sites),
            microtime(true) - $start));
    }

    public function deleteRedisKey(): void
    {
        $this->_container->getShared('redis')->del(self::REDIS_KEY_ALREADY_RUNNING);
    }

    public function isRunning(): bool
    {
        return !($this->_container->getShared('redis')->get(self::REDIS_KEY_ALREADY_RUNNING) === false);
    }

    protected static function _getStrategy(string $name, Ourprice $item): AbstractStrategy
    {
        $className = __NAMESPACE__ . '\\Strategies\\' . $name;
        /** @var AbstractStrategy $strategy */
        return new $className($item);
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    protected function _saveXls(SearchInetSite $site): void
    {
        $spreadsheet = new Spreadsheet();
        $sheetPrice = $spreadsheet->getActiveSheet();
        $sheetError = new Worksheet($spreadsheet, 'error');
        $spreadsheet->addSheet($sheetError);

        $sheetPrice->getColumnDimension('A')->setWidth(12);
        $sheetPrice->getColumnDimension('B')->setWidth(48);
        $sheetPrice->setCellValue('A1', 'Курс = ' . $this->_usdRate);
        $sheetPrice->setCellValue('C1', 'Обновлено ' . $site->update_date);

        $indexPrice = 2;
        $indexError = 1;
        foreach (SearchInetFound::findBySiteIdForSearchInet($site->id)->toArray() as $row) {
            if ($row['code'] !== 'error') {
                $sheetPrice->fromArray($row, null, 'A' . $indexPrice++);
            } else {
                $sheetError->setCellValue('A' . $indexError++, $row['name']);
            }
        }

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save(self::PRICE_PATH . $site->name . '.xls');
        $spreadsheet->disconnectWorksheets();
    }

    protected function _saveTxt(SearchInetSite $site): void
    {
        $fileResult = '';
        $fileError = '';
        foreach (SearchInetFound::findBySiteIdForSearchInet($site->id)->toArray() as $row) {
            if ($row['code'] !== 'error') {
                foreach($row as $value) {
                    $fileResult .= $value . "\n";
                }
            } else {
                $fileError .= $row['name'] . "\n";
            }
        }
        file_put_contents(self::PRICE_PATH . $site->name . '.txt', $fileResult);
        file_put_contents(self::PRICE_PATH . $site->name . '.err', $fileError);
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws Exception
     */
    protected function _saveSites()
    {
        foreach ($this->_sites as $site) {
            self::_saveXls($site);
            self::_saveTxt($site);
            $site->update_date = date('Y-m-d H:i:s');
            if (! $site->save()) {
                throw new Exception($site->getMessages());
            }
        }
    }
}