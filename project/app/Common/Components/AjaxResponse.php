<?php
declare(strict_types=1);

namespace App\Common\Components;

class AjaxResponse
{
    protected array $_data;

    protected bool $_success;

    protected string $_message;

    public function send(): bool
    {
        $out = [];
        $out['success'] = $this->_success;
        $out['data'] = $this->_data;
        $out['message'] = $this->_message;
        echo json_encode($out);
        return false;
    }

    public function success(): self
    {
        $this->_success = true;
        return $this;
    }

    public function fail(string $message = ''): self
    {
        $this->_success = false;
        $this->_message = $message;
        return $this;
    }

    public function with(array $data): self
    {
        $this->_data = $data;
        return $this;
    }
}