<?php
declare(strict_types=1);

namespace App\Common\Components;

use Phalcon\Di\Injectable;

class QueryUrl extends Injectable
{
    protected array $_params;

    protected array $_defaultParams;

    protected string $_url;

    public function __construct()
    {
        $this->_params = $this->request->getQuery();
        $this->_url = $this->_params['_url'];
        unset($this->_params['_url']);
        $this->_defaultParams = $this->_params;
    }

    public function getUrl(array $paramsToChange = []): string
    {
        $out = $this->url->get($this->_url, array_merge($this->_params, $paramsToChange));
        $this->setDefault();
        return $out;

    }

    public function set(string $name, string $value): self
    {
        $this->_params[$name] = $value;
        return $this;
    }

    public function remove(string $name): self
    {
        unset($this->_params[$name]);
        return $this;
    }

    public function setDefault(): self
    {
        $this->_params = $this->_defaultParams;
        return $this;
    }
}