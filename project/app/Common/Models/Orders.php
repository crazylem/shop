<?php
declare(strict_types=1);

namespace App\Common\Models;

use Exception;

/**
 * @method static findFirstById($id)
 */
class Orders extends Base\Orders
{
    /**
     * @throws Exception
     */
    public function saveCartToOrder(): void
    {
        $userId = $this->getDI()->get('session')->get('auth')['id'];
        if ($cartItems = Cart::findByUserId($userId)) {
            $order = new self();
            $order->user_id = $userId;
            $order->date = date('Y-m-d H:i:s');
            try {
                $order->save();
                foreach ($cartItems as $cartItem) {
                    self::_saveCartItemToOrdersItems($cartItem, $order);
                }
                $cartItems->delete();
            } catch (Exception $e) {
                $ordersItems = $order->getRelated('OrdersItems');
                if ($ordersItems !== false) {
                    foreach ($ordersItems as $orderItem) {
                        $orderItem->delete();
                    }
                }
                $order->delete();
                throw $e;
             }
        }
    }

    /**
     * @throws Exception
     */
    public static function getNotUploaded(): string
    {
        $orders = self::find(['reserve IS NULL']);
        $out = count($orders) . "\n";
        foreach ($orders as $order) {
            /** @var Users $user */
            $user = $order->getRelated('users');
            if (is_null($user->id_1c)) {
                throw new Exception('id in 1C is not set. user_id = ' . $user->id);
            }
            $out .= sprintf("%d\n%d\n", $order->id, $user->id_1c);
            /** @var OrdersItems[] $items */
            $items = $order->getRelated('OrdersItems');
            $out .= count($items) . "\n";
            foreach ($items as $item) {
                $out .= sprintf("%d\n%f\n%d\n", $item->item_id, $item->price, $item->quantity);
            }
        }
        return $out;
    }

    public static function setUploaded(int $id, string $reserve): void
    {
        if ($order = self::findFirstById($id)) {
            $order->reserve = $reserve;
            $order->save();
        }
    }

    /**
     * @throws Exception
     */
    protected static function _saveCartItemToOrdersItems(Cart $cartItem, Orders $order): void
    {
        $orderItem = new OrdersItems();
        $orderItem->Orders = $order;
        $orderItem->item_id = $cartItem->item_id;
        $orderItem->quantity = $cartItem->quantity;
        $item = Ourprice::findFirstById($cartItem->item_id);
        $orderItem->price = $item->{(new Ourprice())->getPriceColumnName()};
        $orderItem->save();
    }
}