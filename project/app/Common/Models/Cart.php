<?php
declare(strict_types=1);

namespace App\Common\Models;

use Phalcon\Mvc\ModelInterface;
use Phalcon\Mvc\Model\ResultsetInterface;
use Phalcon\Mvc\Model\Query\BuilderInterface;

/**
 * Class Cart
 * @package App\Common\Models
 *
 * @method static Cart[]|ResultsetInterface findByUserId(int $id)
 */
class Cart extends Base\Cart
{
    public function findFirstByItemId(int $itemId): Base\Cart|ModelInterface
    {
        return self::findFirst([
            'user_id = :userId: AND item_id = :itemId:',
            'bind' => [
                'userId' => $this->getDI()->get('session')->get('auth')['id'],
                'itemId' => $itemId,
            ]
        ]);
    }

    public function getBuilder(): BuilderInterface
    {
        return $this->_createBaseBuilder()
            ->orderBy('item');
    }

    public function getNumberAndAmount(): array
    {
        return $this->_createBaseBuilder()
            ->columns([
                'COUNT(c.id) as number',
                sprintf('SUM(o.%s * c.quantity) as amount', (new Ourprice())->getPriceColumnName()),
            ])
            ->getQuery()
            ->execute()
            ->toArray()[0];
    }

    protected function _createBaseBuilder(): BuilderInterface
    {
        return self::createBuilder()
            ->columns([
                'id' => 'o.id',
                'item' => 'o.item',
                'price' => 'o.' . (new Ourprice())->getPriceColumnName(),
                'priceUah' => sprintf('o.%s*%s', (new Ourprice())->getPriceColumnName(), UsdRate::getLast()),
                'warranty' => 'o.warranty',
                'stock' => 'o.inventory',
                'cart' => 'c.quantity',
            ])
            ->from(['c' => self::class])
            ->leftJoin(Ourprice::class, 'c.item_id = o.id', 'o')
            ->where('c.user_id = :userId:', ['userId' => $this->getDI()->get('session')->get('auth')['id']]);
    }
}