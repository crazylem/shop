<?php
declare(strict_types=1);

namespace App\Common\Models;

use Phalcon\Di\Di;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query\BuilderInterface;

class ModelBase extends Model
{
    public static function createBuilder(): BuilderInterface
    {
        return Di::getDefault()->getShared('modelsManager')->createBuilder();
    }
}