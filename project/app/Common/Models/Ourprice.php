<?php
declare(strict_types=1);

namespace App\Common\Models;

use Exception;
use JetBrains\PhpStorm\ArrayShape;
use Phalcon\Mvc\ModelInterface;
use Phalcon\Mvc\Model\Query\BuilderInterface;
use Phalcon\Mvc\Model\ResultsetInterface;
use Phalcon\Mvc\Model\Resultset\Simple as ResultsetSimple;

/**
 * Class Ourprice
 * @package App\Common\Models
 * @method static Ourprice[] findBySearchInet(int $int)
 */
class Ourprice extends Base\Ourprice
{
    public static function findFirstById(int $id): Base\Ourprice|ModelInterface|null
    {
        return parent::findFirst([
            'id = ?0 AND active = 1',
            'bind' => [$id],
        ]);
    }

    /**
     * @throws Exception
     */
    public static function updateFromTsv(string $tsv): int
    {
        $keys = ['item', 'warranty', 'price', 'bulkPrice', 'bulkPrice2', 'description', 'id', 'pid', 'www', 'folderName',
            'inventory', 'order', 'active', 'keyword1', 'keyword2', 'keyword3', 'keyword_not', 'search_inet'];
        $values = explode("\t", $tsv);
        if (! $dataColumnMap = array_combine($keys, $values)) {
            throw new Exception('array_combine error');
        }
        if (! $ourprice = self::findFirstById((int) $dataColumnMap['id'])) {
            $ourprice = new self();
        }
        $ourprice->assign($dataColumnMap);
        if ($ourprice->save() === false) {
            throw new Exception(implode('. ', $ourprice->getMessages()));
        }
        return (int) $ourprice->id;
    }

    public function getBuilderForCatalog(int $pid): BuilderInterface
    {
        return $this->_createBaseBuilder('item')
            ->andWhere('o.pid = :pid:', ['pid' => $pid]);
    }

    public static function getOpenedFolders(int $id): array
    {
        $out = [];
        $pid = $id;
        while ($entity = self::findFirstById($pid)) {
            $out[] = $entity->id;
            $pid = (int) $entity->pid;
        }
        return $out;
    }

    public static function findFoldersByPid(int $pid): ResultsetInterface|array|Base\Ourprice
    {
        return parent::find([
            'pid = ?0 AND folderName != "" AND active = 1',
            'bind' => [$pid],
            'order' => '"order" DESC, folderName',
        ]);
    }

    public function getBuilderForSearch(array $params): BuilderInterface
    {
        $builder = $this->_createBaseBuilder('item');
        self::_addConditionForSearch($builder, $params['searchText']);
        if ($params['folder'] !== 0) {
            $builder->andWhere('o.pid = :folder:', ['folder' => $params['folder']]);
        }
        return $builder;
    }

    public function getFoundQuantityInFolders(string $searchText): array
    {
        $out = [];
        $connection = $this->getDI()->get('db');

        $builder = self::createBuilder()
            ->columns([
                'id'  => 'f.id',
                'pid' => 'f.pid',
                'qty' => 'COUNT(o.id)',])
            ->from(['o' => self::class])
            ->leftJoin(self::class, 'o.pid = f.id', 'f')
            ->where('o.active = 1 AND f.active = 1');
        self::_addConditionForSearch($builder, $searchText);
        $builder->groupBy('f.id');

        $result = self::_getSubqueryAndQtyInFolders($builder->getQuery()->execute());
        $out += $result['quantityInFoldersForCurrentHierarchyLevel'];

        while ($result['subqueryForNextHierarchyLevel'] !== '') {
            $query = sprintf(
                'SELECT f.id, f.pid, SUM(q.qty) AS qty FROM (%s) AS q LEFT JOIN ourprice f ON q.pid = f.id GROUP BY f.id',
                $result['subqueryForNextHierarchyLevel']);
            $result = self::_getSubqueryAndQtyInFolders($connection->query($query)->fetchAll());
            $out += $result['quantityInFoldersForCurrentHierarchyLevel'];
        }

        return $out;
    }

    public function getPriceColumnName(): string
    {
        $category = $this->getDI()->get('session')->get('auth')['category'];
        if ($category === 2) {
            return 'bulkPrice';
        }
        if ($category === 3) {
            return 'bulkPrice2';
        }
        return 'price';
    }

    #[ArrayShape([
        'subqueryForNextHierarchyLevel' => 'string',
        'quantityInFoldersForCurrentHierarchyLevel' => 'array'
    ])]
    protected static function _getSubqueryAndQtyInFolders(array|ResultsetSimple $rows): array
    {
        $query = '';
        $qtyInFolders = [];
        foreach ($rows as $row) {
            $qtyInFolders[$row['id']] = $row['qty'];
            if ((int) $row['pid'] !== 0) {
                $query .= $query === '' ? '' : ' UNION ';
                $query .= sprintf('SELECT %u AS id, %u AS pid, %u AS qty', $row['id'], $row['pid'], $row['qty']);
            }
        }
        return [
            'subqueryForNextHierarchyLevel' => $query,
            'quantityInFoldersForCurrentHierarchyLevel' => $qtyInFolders,
        ];
    }

    protected static function _addConditionForSearch(BuilderInterface $builder, string $searchText): BuilderInterface
    {
        $index = 1;
        $needles = explode(' ', $searchText);
        foreach($needles as $needle) {
            $builder->andWhere('o.item LIKE :needle' . $index . ':', ['needle' . $index => '%' . $needle . '%']);
            $index++;
        }
        return $builder;
    }

    protected function _createBaseBuilder(string $orderBy): BuilderInterface
    {
        return self::createBuilder()
            ->columns([
                'id' => 'o.id',
                'item' => 'o.item',
                'price' => 'o.' . $this->getPriceColumnName(),
                'priceUah' => sprintf('o.%s*%f', $this->getPriceColumnName(), UsdRate::getLast()),
                'warranty' => 'o.warranty',
                'stock' => 'o.inventory',
                'cart' => 'c.quantity',])
            ->from(['o' => self::class])
            ->leftJoin(Cart::class, 'o.id = c.item_id', 'c')
            ->where(
                'c.user_id IS NULL OR c.user_id = :userId:',
                ['userId' => $this->getDI()->get('session')->get('auth')['id']])
            ->andWhere('o.active = 1')
            ->orderBy($orderBy);
    }
}
