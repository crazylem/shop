<?php
declare(strict_types=1);

namespace App\Common\Models;

use Phalcon\Mvc\Model\ResultsetInterface;

/**
 * @method static SearchInetSite[] | ResultsetInterface findByActive(int $active)
 */
class SearchInetSite extends Base\SearchInetSite
{

}