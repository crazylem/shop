<?php

namespace App\Common\Models\Base;

class Cart extends \App\Common\Models\ModelBase
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $item_id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var integer
     */
    public $quantity;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("shop");
        $this->setSource("cart");
        $this->belongsTo('item_id', 'App\Common\Models\Base\Ourprice', 'id', ['alias' => 'Ourprice']);
        $this->belongsTo('user_id', 'App\Common\Models\Base\Users', 'id', ['alias' => 'Users']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Cart[]|Cart|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Cart|\Phalcon\Mvc\ModelInterface
     */
    public static function findFirst($parameters = null): Cart|\Phalcon\Mvc\ModelInterface
    {
        return parent::findFirst($parameters);
    }

}
