<?php

namespace App\Common\Models\Base;

class Ourprice extends \App\Common\Models\ModelBase
{

    /**
     *
     * @var string
     */
    public $item;

    /**
     *
     * @var string
     */
    public $warranty;

    /**
     *
     * @var string
     */
    public $price;

    /**
     *
     * @var string
     */
    public $bulkPrice;

    /**
     *
     * @var string
     */
    public $bulkPrice2;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $pid;

    /**
     *
     * @var string
     */
    public $www;

    /**
     *
     * @var string
     */
    public $folderName;

    /**
     *
     * @var string
     */
    public $inventory;

    /**
     *
     * @var integer
     */
    public $order;

    /**
     *
     * @var integer
     */
    public $active;

    /**
     *
     * @var string
     */
    public $keyword1;

    /**
     *
     * @var string
     */
    public $keyword2;

    /**
     *
     * @var string
     */
    public $keyword3;

    /**
     *
     * @var string
     */
    public $keyword_not;

    /**
     *
     * @var integer
     */
    public $search_inet;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("shop");
        $this->setSource("ourprice");
        $this->hasMany('id', 'App\Common\Models\Base\Cart', 'item_id', ['alias' => 'Cart']);
        $this->hasMany('id', 'App\Common\Models\Base\OrdersItems', 'item_id', ['alias' => 'OrdersItems']);
        $this->hasMany('id', 'App\Common\Models\Base\SearchInetFound', 'item_id', ['alias' => 'SearchInetFound']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Ourprice[]|Ourprice|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Ourprice|\Phalcon\Mvc\ModelInterface|null
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
