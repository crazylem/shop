<?php

namespace App\Common\Models\Base;

class SearchInetFound extends \App\Common\Models\ModelBase
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $search_inet_site_id;

    /**
     *
     * @var integer
     */
    public $item_id;

    /**
     *
     * @var string
     */
    public $code;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $price;

    /**
     *
     * @var string
     */
    public $stock;

    /**
     *
     * @var string
     */
    public $warranty;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("shop");
        $this->setSource("search_inet_found");
        $this->belongsTo('item_id', 'App\Common\Models\Base\Ourprice', 'id', ['alias' => 'Ourprice']);
        $this->belongsTo('search_inet_site_id', 'App\Common\Models\Base\SearchInetSite', 'id', ['alias' => 'SearchInetSite']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SearchInetFound[]|SearchInetFound|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SearchInetFound|\Phalcon\Mvc\ModelInterfacfe
     */
    public static function findFirst($parameters = null): ?\Phalcon\Mvc\ModelInterface
    {
        return parent::findFirst($parameters);
    }

}
