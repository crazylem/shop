<?php

namespace App\Common\Models\Base;

class OrdersItems extends \App\Common\Models\ModelBase
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $order_id;

    /**
     *
     * @var integer
     */
    public $item_id;

    /**
     *
     * @var string
     */
    public $price;

    /**
     *
     * @var integer
     */
    public $quantity;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("shop");
        $this->setSource("orders_items");
        $this->belongsTo('order_id', 'App\Common\Models\Base\Orders', 'id', ['alias' => 'Orders']);
        $this->belongsTo('item_id', 'App\Common\Models\Base\Ourprice', 'id', ['alias' => 'Ourprice']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return OrdersItems[]|OrdersItems|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return OrdersItems|\Phalcon\Mvc\ModelInterface
     */
    public static function findFirst($parameters = null): ?\Phalcon\Mvc\ModelInterface
    {
        return parent::findFirst($parameters);
    }

}
