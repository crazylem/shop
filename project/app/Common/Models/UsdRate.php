<?php
declare(strict_types=1);

namespace App\Common\Models;

use Exception;

class UsdRate extends Base\UsdRate
{
    public static function getLast(): float
    {
        if ($model = self::findFirst(['order' => 'date desc'])) {
            return $model->rate;
        }
        return 1.0;
    }

    /**
     * @throws Exception
     */
    public static function updateRate(string $rate)
    {
        $model = new self([
            'rate' => $rate,
            'date' => date('Y-m-d H:i:s'),
        ]);
        if (! $model->create()) {
            throw new Exception(implode($model->getMessages()));
        }
    }
}
