<?php
declare(strict_types=1);

namespace App\Common\Models;

use Exception;
use Phalcon\Mvc\Model\ResultsetInterface;

class SearchInetFound extends Base\SearchInetFound
{
    public static function findBySiteIdForSearchInet(int $siteId): ResultsetInterface
    {
        return self::createBuilder()
            ->columns([
                'code' => 'sif.code',
                'name' => 'sif.name',
                'price' => 'sif.price',
                'stock' => 'sif.stock',
                'warranty' => 'sif.warranty',
            ])
            ->from(['sif' => self::class])
            ->where('sif.search_inet_site_id = :siteId:', ['siteId' => $siteId])
            ->orderBy('name asc')
            ->getQuery()
            ->execute();
    }

    /**
     * @throws Exception
     */
    public static function deleteAll(): void
    {
        $result = self::find();
        if (! $result->delete()) {
            throw new Exception($result->getMessages());
        }

    }

    /**
     * @throws Exception
     */
    public static function createRecord(array $data)
    {
        $model = new self();
        $model->assign($data);
        if (! $model->create()) {
            throw new Exception($model->getMessages());
        }
    }
}