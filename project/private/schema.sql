/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_cart_ourprice` (`item_id`),
  KEY `FK_cart_users` (`user_id`),
  CONSTRAINT `FK_cart_ourprice` FOREIGN KEY (`item_id`) REFERENCES `ourprice` (`id`),
  CONSTRAINT `FK_cart_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;


DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `reserve` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_orders_users` (`user_id`),
  CONSTRAINT `FK_orders_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;


DROP TABLE IF EXISTS `orders_items`;
CREATE TABLE IF NOT EXISTS `orders_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `price` double(53,2) NOT NULL DEFAULT 0.00,
  `quantity` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_orders_items_orders` (`order_id`),
  KEY `FK_orders_items_orders_items` (`item_id`),
  CONSTRAINT `FK_orders_items_orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `FK_orders_items_ourprice` FOREIGN KEY (`item_id`) REFERENCES `ourprice` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ourprice`;
CREATE TABLE IF NOT EXISTS `ourprice` (
  `item` varchar(255) DEFAULT NULL,
  `warranty` varchar(4) DEFAULT NULL,
  `price` double(53,2) DEFAULT NULL,
  `bulkPrice` double(53,2) DEFAULT NULL,
  `bulkPrice2` double(53,2) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `id` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned DEFAULT NULL,
  `www` tinytext DEFAULT NULL,
  `folderName` varchar(100) DEFAULT NULL,
  `inventory` char(1) DEFAULT NULL,
  `order` int(3) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `keyword1` varchar(100) DEFAULT NULL,
  `keyword2` varchar(100) DEFAULT NULL,
  `keyword3` varchar(100) DEFAULT NULL,
  `keyword_not` varchar(100) DEFAULT NULL,
  `search_inet` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40000 ALTER TABLE `ourprice` ENABLE KEYS */;

DROP TABLE IF EXISTS `search_inet_found`;
CREATE TABLE IF NOT EXISTS `search_inet_found` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `search_inet_site_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` double(53,2) unsigned DEFAULT 0.00,
  `stock` varchar(50) DEFAULT NULL,
  `warranty` smallint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_search_inet_found_search_inet_site` (`search_inet_site_id`),
  KEY `FK_search_inet_found_ourprice` (`item_id`),
  CONSTRAINT `FK_search_inet_found_ourprice` FOREIGN KEY (`item_id`) REFERENCES `ourprice` (`id`),
  CONSTRAINT `FK_search_inet_found_search_inet_site` FOREIGN KEY (`search_inet_site_id`) REFERENCES `search_inet_site` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `search_inet_found` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_inet_found` ENABLE KEYS */;

DROP TABLE IF EXISTS `search_inet_site`;
CREATE TABLE IF NOT EXISTS `search_inet_site` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `search_inet_site` DISABLE KEYS */;
INSERT INTO `search_inet_site` (`id`, `name`, `active`, `update_date`) VALUES
	(1, 'Rozetka', 0, NULL),
	(2, 'Eldorado', 0, NULL),
	(3, 'Comfy', 0, NULL),
	(4, 'Moyo', 0, NULL),
    (5, 'Foxtrot', 0, NULL),
    (6, 'Allo', 0, NULL);
/*!40000 ALTER TABLE `search_inet_site` ENABLE KEYS */;

DROP TABLE IF EXISTS `usd_rate`;
CREATE TABLE IF NOT EXISTS `usd_rate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rate` double(53,2) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL,
  `email` char(50) NOT NULL,
  `password` char(60) NOT NULL,
  `category` int(1) unsigned NOT NULL DEFAULT 1,
  `id_1c` mediumint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `category`, `id_1c`) VALUES
	(1, 'kostya', 'konstantin.lemehov@gmail.com', '$2y$08$UDZZSjRPYmo3KzVpTFJpYeKI9aoHDxqOV/McchlmVQAGNHV9VDOGu', 3, 436);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
