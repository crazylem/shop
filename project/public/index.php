<?php
declare(strict_types=1);

use Phalcon\Logger\Logger;


error_reporting(E_ALL);

define('DIR_BASE', dirname(__DIR__));

require_once '../vendor/autoload.php';

$isApi = preg_match('/^\/api\/.*/i', $_SERVER['REQUEST_URI']) === 1;
$bootstrap = null;
try {
    if ($isApi) {
        $bootstrap = new App\BootstrapApi();
    } else {
        $bootstrap = new App\BootstrapGui();
    }
    $bootstrap->initialize();
    $response = $bootstrap->getApplication()->handle($_SERVER['REQUEST_URI']);
    if (! $isApi) $response->send();
} catch (Throwable  $e) {
    $app = $bootstrap?->getApplication();
    $container = $app?->getDI();
    if ($container?->has('logger')) {
        /** @var Logger $logger */
        $logger = $container->get('logger');
        /** @noinspection PhpUnhandledExceptionInspection */
        $logger->error(sprintf('%s in file %s in line #%d', $e->getMessage(), $e->getFile(), $e->getLine()));
        if ($isApi) {
            $app->response
                ->setStatusCode(400)
                ->send();
        } else {
            echo 'error 404';
        }
    } else {
        echo 'something wrong happened<br>';
    }
    if ($_ENV['DEBUG'] == 1) {
        echo $e->getMessage();
    }
}
