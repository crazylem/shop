jQuery.noConflict();

(function($) {
    'use strict';

    $('.quantity').change(function() {
        let $numberAndAmount = $('#numberAndAmount');
        let $tr = $(this).closest('tr');
        let id = $tr.data('id');
        let $qty = $(this);
        $.ajax({
            'url': '/cart/update/',
            'method': 'POST',
            'data': {
                'itemId': id,
                'quantity': $qty.val(),
            }
        })
            .done(function(data) {
                let response = JSON.parse(data);
                if (response.success) {
                    $qty.val(response.data.quantity);
                    $numberAndAmount.text(response.data.numberAndAmount);
                } else {
                    console.log(response.message);
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log('ajax fail');
            })
    });
}(jQuery));