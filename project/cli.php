<?php
declare(strict_types=1);

use Phalcon\Logger\Logger;

const DIR_BASE = __DIR__;

require_once 'vendor/autoload.php';

$arguments = [];
foreach ($argv as $k => $arg) {
    if ($k === 1) {
        $arguments['task'] = $arg;
    } elseif ($k === 2) {
        $arguments['action'] = $arg;
    } elseif ($k >= 3) {
        $arguments['params'][] = $arg;
    }
}

$bootstrap = null;
try {
    $bootstrap = new App\BootstrapCli();
    $bootstrap->initialize();
    $bootstrap->getApplication()->handle($arguments);
} catch (Exception $e) {
    $app = $bootstrap?->getApplication();
    $container = $app?->getDI();
    if ($container?->has('logger')) {
        /** @var Logger $logger */
        $logger = $container->get('logger');
        /** @noinspection PhpUnhandledExceptionInspection */
        $logger->error(sprintf('%s in file %s in line #%d', $e->getMessage(), $e->getFile(), $e->getLine()));
    }
    fwrite(STDERR, $e->getMessage() . PHP_EOL);
    exit(1);
}