<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Migrations\Mvc\Model\Migration;

/**
 * Class OurpriceMigration_101
 */
class OurpriceMigration_101 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('ourprice', [
                'columns' => [
                    new Column(
                        'item',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => false,
                            'size' => 100,
                            'first' => true
                        ]
                    ),
                    new Column(
                        'warranty',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => false,
                            'size' => 4,
                            'after' => 'item'
                        ]
                    ),
                    new Column(
                        'price',
                        [
                            'type' => Column::TYPE_DOUBLE,
                            'notNull' => false,
                            'scale' => 2,
                            'after' => 'warranty'
                        ]
                    ),
                    new Column(
                        'bulkPrice',
                        [
                            'type' => Column::TYPE_DOUBLE,
                            'notNull' => false,
                            'scale' => 2,
                            'after' => 'price'
                        ]
                    ),
                    new Column(
                        'bulkPrice2',
                        [
                            'type' => Column::TYPE_DOUBLE,
                            'notNull' => false,
                            'scale' => 2,
                            'after' => 'bulkPrice'
                        ]
                    ),
                    new Column(
                        'description',
                        [
                            'type' => Column::TYPE_LONGTEXT,
                            'notNull' => false,
                            'after' => 'bulkPrice2'
                        ]
                    ),
                    new Column(
                        'id',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 10,
                            'after' => 'description'
                        ]
                    ),
                    new Column(
                        'pid',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => false,
                            'size' => 10,
                            'after' => 'id'
                        ]
                    ),
                    new Column(
                        'www',
                        [
                            'type' => Column::TYPE_TINYTEXT,
                            'notNull' => false,
                            'after' => 'pid'
                        ]
                    ),
                    new Column(
                        'folderName',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => false,
                            'size' => 100,
                            'after' => 'www'
                        ]
                    ),
                    new Column(
                        'inventory',
                        [
                            'type' => Column::TYPE_CHAR,
                            'notNull' => false,
                            'size' => 1,
                            'after' => 'folderName'
                        ]
                    ),
                    new Column(
                        'order',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => false,
                            'size' => 3,
                            'after' => 'inventory'
                        ]
                    )
                ],
                'indexes' => [
                    new Index('PRIMARY', ['id'], 'PRIMARY')
                ],
                'options' => [
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8mb3_general_ci'
                ],
            ]
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        $this->batchInsert('ourprice', [
                'item',
                'warranty',
                'price',
                'bulkPrice',
                'bulkPrice2',
                'description',
                'id',
                'pid',
                'www',
                'folderName',
                'inventory',
                'order'
            ]
        );
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
        $this->batchDelete('ourprice');
    }

}
