<?php
declare(strict_types=1);

namespace Tests\Unit\Components\SearchInet\Strategies;

use ReflectionException;
use PHPUnit\Framework\TestCase;
use App\Common\Components\SearchInet\Strategies\AbstractStrategy;
use App\Common\Models\Ourprice;
use Tests\Unit\PhpUnitUtil;

class AbstractStrategyTest extends TestCase
{
    /**
     * @dataProvider getHasKeywordNotFunctionProvider
     * @throws ReflectionException
     */
    public function testHasKeywordNotFunction(Ourprice $item, string $name, bool $returnValue)
    {
        $mockAbstractStrategy = $this->getMockForAbstractClass(AbstractStrategy::class, [$item]);
        $this->assertEquals($returnValue, PhpUnitUtil::callMethod($mockAbstractStrategy, '_hasKeywordNot', [$name]));
    }

    public function getHasKeywordNotFunctionProvider(): array
    {
        $name = 'asdfqwerzxcv';

        $mockItem = $this->getMockBuilder(Ourprice::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mockItem->keyword_not = '';
        $out['Empty keyword_not'] = [$mockItem, $name, false];

        $mockItem = clone $mockItem;
        $mockItem->keyword_not = 'asdf';
        $out['Name has keyword_not'] = [$mockItem, $name, true];

        $mockItem = clone $mockItem;
        $mockItem->keyword_not = 'fdsa';
        $out['Name has not keyword_not'] = [$mockItem, $name, false];

        return $out;
    }
}