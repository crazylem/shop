<?php
declare(strict_types=1);

namespace Tests\Unit;

use ReflectionException, ReflectionClass;

class PhpUnitUtil
{
    /**
     * @throws ReflectionException
     */
    public static function callMethod(object $obj, string $name, array $args): mixed
    {
        $class = new ReflectionClass($obj);
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method->invokeArgs($obj, $args);
    }
}